# Whack-a-mole simulator

## Description
The purpose of this project was to rewrite and improve an older project which was a part of
the course 'Computer Graphics' at the Faculty of Mathematics.

It was also used to demonstrate the usage of different tools used for improving C++ projects
as a part of the course 'Tools for Software Development' at the Faculty of Mathematics. 

## Original repository

https://github.com/MATF-RG19/RG148-whack-a-mole-simulator

## Building the app

### Using Makefile
Run the following commands in terminal:

``` > cd src```

``` > make```

``` > ./Whack-A-Mole```

To remove build files run:
``` make clean```

### Using cmake 
Run the following commands in terminal:
``` > mkdir build ```

``` > cd build ```

``` > cmake .. ```

``` > make ```

``` ./Whack-a-mole ```

To build the project with clang-tidy checks run:

``` > cmake -DCMAKE_EXPORT_COMPILE_COMMANDS=ON .. ```
