#include "../include/Callbacks.hpp"

#include <GL/glut.h>

#include "../include/Animations.hpp"
#include "../include/Drawing.hpp"
#include "../include/GlobalVariables.hpp"

void on_keyboard(unsigned char key, int x, int y) {
    /*pritiskom odgovaracucjeg broja
     palica se pokrece do te rupe i udara iznad nje,
     a ako je kuglica izasla iz te rupe u tom trenutku
     povecava se broj poena*/
    switch (key) {
        case '1':
            if (!keyboard_active) {
                wanted_angle = 90;
                step = 3;
                glutTimerFunc(TIMER_INTERVAL2, on_timer2, TIMER_ID2);
                keyboard_active = 1;
                if (current_hole == 1) {
                    score++;
                }
            }
            break;
        case '2':
            if (!keyboard_active) {
                wanted_angle = 60;
                step = 2;
                glutTimerFunc(TIMER_INTERVAL2, on_timer2, TIMER_ID2);
                keyboard_active = 1;
                if (current_hole == 2)
                    score++;
            }
            break;
        case '3':
            if (!keyboard_active) {
                wanted_angle = 30;
                step = 1;
                glutTimerFunc(TIMER_INTERVAL2, on_timer2, TIMER_ID2);
                keyboard_active = 1;
                if (current_hole == 3)
                    score++;
            }
            break;
        case '4':
            if (!keyboard_active) {
                wanted_angle = 90;
                step = 3;
                wanted_z_bat = -2.1;
                translate_step = 0.04;
                glutTimerFunc(TIMER_INTERVAL2, on_timer2, TIMER_ID2);
                keyboard_active = 1;
                if (current_hole == 4)
                    score++;
            }
            break;
        case '5':
            if (!keyboard_active) {
                wanted_angle = 60;
                step = 2;
                wanted_z_bat = -2.1;
                translate_step = 0.04;
                glutTimerFunc(TIMER_INTERVAL2, on_timer2, TIMER_ID2);
                keyboard_active = 1;
                if (current_hole == 5)
                    score++;
            }

            break;
        case '6':
            if (!keyboard_active) {
                wanted_angle = 30;
                step = 1;
                wanted_z_bat = -2.1;
                translate_step = 0.04;
                glutTimerFunc(TIMER_INTERVAL2, on_timer2, TIMER_ID2);
                keyboard_active = 1;
                if (current_hole == 6)
                    score++;
            }
            break;
        case '7':
            if (!keyboard_active) {
                wanted_angle = 90;
                step = 3;
                wanted_z_bat = -0.6;
                translate_step = 0.09;
                glutTimerFunc(TIMER_INTERVAL2, on_timer2, TIMER_ID2);
                keyboard_active = 1;
                if (current_hole == 7)
                    score++;
            }
            break;
        case '8':
            if (!keyboard_active) {
                wanted_angle = 60;
                step = 2;
                wanted_z_bat = -0.6;
                translate_step = 0.09;
                glutTimerFunc(TIMER_INTERVAL2, on_timer2, TIMER_ID2);
                keyboard_active = 1;
                if (current_hole == 8)
                    score++;
            }
            break;
        case '9':
            if (!keyboard_active) {
                wanted_angle = 30;
                step = 1;
                wanted_z_bat = -0.6;
                translate_step = 0.09;
                glutTimerFunc(TIMER_INTERVAL2, on_timer2, TIMER_ID2);
                keyboard_active = 1;
                if (current_hole == 9)
                    score++;
            }
            break;
        case 27:
            exit(0);
        default:
            break;
    }
}

void on_mouse(int button, int state, int x, int y) {
    switch (button) {
        case GLUT_LEFT_BUTTON:
            if (!animation_ongoing) {
                glutTimerFunc(TIMER_INTERVAL, on_timer, TIMER_ID);
                animation_ongoing = 1;
            }
            break;
        case GLUT_RIGHT_BUTTON:
            animation_ongoing = 0;
            if (score - level - 1 > highScore)
                highScore = score - level - 1;
            score = 1;
            level = 0;
            ball_speed = 0.04;
            break;
        default:
            break;
    }
}

void on_display() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    drawUI();

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(0, 5, -3.5, 0, 0, 0, 0, 1, 0);

    // iscrtavanje platforme
    glPushMatrix();
    GLfloat ambient3[] = {1, 0.78, 0.35, 0};
    GLfloat diffuse3[] = {0.3, 0.3, 0, 0, 0};
    GLfloat specular3[] = {0.6, 0.6, 0.6, 0};
    GLfloat shininess3 = 80;

    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, ambient3);
    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, diffuse3);
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, specular3);
    glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, shininess3);
    glBegin(GL_QUADS);
    glVertex3f(-2.3, 0, -2.3);
    glVertex3f(2.3, 0, -2.3);
    glVertex3f(2.3, 0, 2.3);
    glVertex3f(-2.3, 0, 2.3);
    glEnd();
    glPopMatrix();

    // iscrtavanje krugova
    glPushMatrix();
    GLfloat ambient2[] = {1, 0.62, 0.11, 0};
    GLfloat diffuse2[] = {0.2, 0.2, 0, 0, 0};
    GLfloat specular2[] = {0.3, 0.3, 0.3, 0};
    GLfloat shininess2 = 40;

    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, ambient2);
    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, diffuse2);
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, specular2);
    glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, shininess2);

    drawCircles();
    glPopMatrix();

    // kuglica
    glPushMatrix();
    GLfloat ambient4[] = {0.45, 0.3, 0.1, 0};
    GLfloat diffuse4[] = {0.4, 0.2, 0.2, 0};
    GLfloat specular4[] = {0.3, 0.3, 0.3, 0};
    GLfloat shininess4 = 80;

    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, ambient4);
    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, diffuse4);
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, specular4);
    glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, shininess4);
    glTranslatef(x_current, y_current, z_current);
    glutSolidSphere(0.30, 30, 30);
    glPopMatrix();

    // iscrtavanje palice
    glPushMatrix();
    GLfloat ambient1[] = {0.45, 0.3, 0.1, 0};
    GLfloat diffuse1[] = {0.4, 0.2, 0.2, 0};
    GLfloat specular1[] = {0.25, 0.3, 0.3, 0};
    GLfloat shininess1 = 80;

    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, ambient1);
    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, diffuse1);
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, specular1);
    glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, shininess1);

    drawBat();
    glPopMatrix();

    glutSwapBuffers();
}