#include "../include/Animations.hpp"

#include <GL/glut.h>

#include <random>

#include "../include/GlobalVariables.hpp"

/*vraca random broj 1-9 da bi se odredilo iz koje
rupe izlazi objekat i postavlja globlane
koordinate iznad te rupe*/
int randomHole(int curr_hole) {
    std::random_device rd;
    std::mt19937 gen(rd());

    int upper = 9;
    int lower = 1;

    std::uniform_int_distribution<int> distribution(lower, upper);

    int random_num = distribution(gen);
    while (random_num == current_hole) {
        random_num = distribution(gen);
    }
    current_hole = random_num;

    switch (random_num) {
        case Holes::BOTTOM_LEFT:
            x_current = ball_x_left;
            z_current = -ball_z_top;
            break;
        case Holes::BOTTOM_MIDDLE:
            x_current = ball_middle;
            z_current = -ball_z_top;
            break;
        case Holes::BOTTOM_RIGHT:
            x_current = -ball_x_left;
            z_current = -ball_z_top;
            break;
        case Holes::MIDDLE_LEFT:
            x_current = ball_x_left;
            z_current = ball_middle;
            break;
        case Holes::MIDDLE:
            x_current = ball_middle;
            z_current = ball_middle;
            break;
        case Holes::MIDDLE_RIGHT:
            x_current = -ball_x_left;
            z_current = ball_middle;
            break;
        case Holes::TOP_LEFT:
            x_current = ball_x_left;
            z_current = ball_z_top;
            break;
        case Holes::TOP_MIDDLE:
            x_current = ball_middle;
            z_current = ball_z_top;
            break;
        case Holes::TOP_RIGHT:
            x_current = -ball_x_left;
            z_current = ball_z_top;
            break;
        default:
            break;
    }

    return random_num;
}

// animacija kuglice
void on_timer(int value) {
    if (value != TIMER_ID)
        return;

    /*kada se kuglica popne na odredjenu visinu
      treba da stane i malo se zadrzi tu*/
    if (y_current >= 1 && animation_ongoing) {
        y_stop = true;
    }

    if (y_current >= 1.001 && animation_ongoing) {
        y_down = true;
    }

    /*kada se vrati u pocetni polozaj
     resetujemo relevantne globalne promenljive
     i random biramo sledecu rupu*/
    if (y_current < 0 && animation_ongoing) {
        y_current = 0;
        y_down = false;
        y_stop = false;
        randomHole(current_hole);
    }

    /*povecavamo brzinu kuglice
     sa porastom broja poena ali samo
     do odredjene brzine*/
    if (score % 10 == 0 && ball_speed < 0.06) {
        ball_speed += 0.005;
        score++;
        level++;
    }

    if (y_down) {
        y_current -= ball_speed;
    } else if (y_stop) {
        /*zadrzavanje kuglice u vazduhu je realizovano
         tako sto se ona zapravo pomalo pomera na gore*/
        y_current += 0.0001;
    } else {
        y_current += ball_speed;
    }

    glutPostRedisplay();

    if (animation_ongoing)
        glutTimerFunc(TIMER_INTERVAL, on_timer, TIMER_ID);
}

// animacija palice
void on_timer2(int value) {
    if (value != TIMER_ID2)
        return;

    /*kada postignemo zeljeni ugao
     pocinjemo da je vracamo unazad*/
    if (rot_angle == wanted_angle) {
        step *= -1;
        translate_step *= -1;
    }

    rot_angle += step;
    z_bat += translate_step;

    /*zaustavljamo animaciju i
     resetujemo relevantne globalne promenljive*/
    if (rot_angle <= 0) {
        keyboard_active = 0;
        step *= -1;
        translate_step = 0;
    }

    glutPostRedisplay();

    if (keyboard_active)
        glutTimerFunc(TIMER_INTERVAL2, on_timer2, TIMER_ID2);
}
