#include "../include/Drawing.hpp"

#include <GL/glut.h>

#include <cstdio>

#include "../include/GlobalVariables.hpp"

void drawText(double x, double y, char *s) {
    glRasterPos2d(x, y);

    for (char *c = s; *c != '\0'; c++) {
        glutBitmapCharacter(GLUT_BITMAP_9_BY_15, *c);
    }
}

// ispisivanje teksta
void drawUI() {
    glPushMatrix();
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, 800.0, 600.0, 0, 0, 250);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glDisable(GL_DEPTH_TEST);
    glDisable(GL_LIGHTING);
    glColor3f(1, 1, .8);

    char currentScore[20], currenthighScore[20], currentLevel[20];
    sprintf(currenthighScore, "High score: %i", highScore);
    sprintf(currentLevel, "Level: %i", level);
    sprintf(currentScore, "Score: %i", score - level - 1);
    drawText(10, 590, currentScore);
    drawText(10, 570, currentLevel);
    drawText(10, 550, currenthighScore);

    glEnable(GL_LIGHTING);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(70, 800.0 / 600.0, 0.1, 250);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(0, 5, -3.5, 0, 0, 0, 0, 1, 0);

    glPopMatrix();
}

void drawCircles() {
    // odsecanje
    GLdouble plane0[] = {0, -1, 0, 0};
    glEnable(GL_CLIP_PLANE0);
    glClipPlane(GL_CLIP_PLANE0, plane0);

    float r = 0.5;
    float spacing = 1.6 / 4.0;

    GLUquadricObj *qobj = gluNewQuadric();

    glPushMatrix();
    // iscrtavanje rupa 3-1
    glTranslatef(static_cast<float>(-2.3) + r + spacing, 0, static_cast<float>(-2.3) + r + spacing);
    glColor3f(1, 0, 0);
    gluSphere(qobj, r, 30, 30);

    glTranslatef(2 * r + spacing, 0, 0);
    gluSphere(qobj, r, 30, 30);

    glTranslatef(2 * r + spacing, 0, 0);
    gluSphere(qobj, r, 30, 30);

    // iscrtavanje rupa 4-6
    glTranslatef(0, 0, 2 * r + spacing);
    gluSphere(qobj, r, 30, 30);

    glTranslatef(-(2 * r + spacing), 0, 0);
    gluSphere(qobj, r, 30, 30);

    glTranslatef(-(2 * r + spacing), 0, 0);
    gluSphere(qobj, r, 30, 30);

    // iscrtavanje rupa 9-7
    glTranslatef(0, 0, 2 * r + spacing);
    gluSphere(qobj, r, 30, 30);

    glTranslatef(2 * r + spacing, 0, 0);
    gluSphere(qobj, r, 30, 30);

    glTranslatef(2 * r + spacing, 0, 0);
    gluSphere(qobj, r, 30, 30);

    gluDeleteQuadric(qobj);
    glPopMatrix();

    glDisable(GL_CLIP_PLANE0);
}

void drawBat() {
    glPushMatrix();

    GLUquadricObj *qobj = gluNewQuadric();

    // ovo je za animaciju
    glTranslatef(-1, 0, z_bat);
    glRotatef(rot_angle, 0, 1, 0);

    glRotatef(-40, 0, 1, 0);

    gluCylinder(qobj, 0.1, 0.25, 4, 150, 150);
    glTranslatef(0, 0, 4);

    gluDisk(qobj, 0, 0.25, 30, 30);

    gluDeleteQuadric(qobj);
    glPopMatrix();
}
