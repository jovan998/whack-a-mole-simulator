#include "../include/GlobalVariables.hpp"

int animation_ongoing;
int keyboard_active;
float x_current = 0, y_current = 0, z_current = 0;  // koordinate kuglice
int current_hole =
    5;  // promenljiva koja pamti koja je trenutna rupa da ne bi dolazilo do ponavljanja
float ball_speed = 0.04;

/*globalne promenljive za palicu*/
float step = 5;
float translate_step = 0;
float z_bat = -3.3;   // pocetni polozaj
float wanted_z_bat;   // zeljeni polozaj
float rot_angle = 0;  // ugao rotacije
float wanted_angle;   // zeljeni ugao

bool y_down = false, y_stop = false;  // flagovi za kretanje kuglice
int score = 1, level = 0;
int highScore = 0;

float ball_x_left = 1.4;
float ball_middle = 0;
float ball_z_top = 1.4;
