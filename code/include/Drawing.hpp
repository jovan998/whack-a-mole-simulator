#ifndef DRAWING_HPP
#define DRAWING_HPP

extern void drawText(double x, double y, char *s);
extern void drawUI();
extern void drawCircles();
extern void drawBat(void);

#endif  // DRAWING_HPP
