#ifndef CALLBACKS_HPP
#define CALLBACKS_HPP

extern void on_keyboard(unsigned char key, int x, int y);
extern void on_mouse(int button, int state, int x, int y);
extern void on_display();

#endif  // CALLBACKS_HPP
