#ifndef GLOBAL_VARIABLES_HPP
#define GLOBAL_VARIABLES_HPP

#include <map>

#define TIMER_ID 0
#define TIMER_ID2 0
#define TIMER_INTERVAL 20
#define TIMER_INTERVAL2 10

extern int animation_ongoing;
extern int keyboard_active;
extern float x_current, y_current, z_current;  // koordinate kuglice
extern int
    current_hole;  // promenljiva koja pamti koja je trenutna rupa da ne bi dolazilo do ponavljanja
extern float ball_speed;

/*globalne promenljive za palicu*/
extern float step;
extern float translate_step;
extern float z_bat;         // pocetni polozaj
extern float wanted_z_bat;  // zeljeni polozaj
extern float rot_angle;     // ugao rotacije
extern float wanted_angle;  // zeljeni ugao

extern bool y_down, y_stop;  // flagovi za kretanje kuglice
extern int score, level;
extern int highScore;

extern float ball_x_left;
extern float ball_middle;
extern float ball_z_top;

enum Holes {
    BOTTOM_LEFT = 1,
    BOTTOM_MIDDLE = 2,
    BOTTOM_RIGHT = 3,
    MIDDLE_LEFT = 4,
    MIDDLE = 5,
    MIDDLE_RIGHT = 6,
    TOP_LEFT = 7,
    TOP_MIDDLE = 8,
    TOP_RIGHT = 9
};

#endif  // GLOBAL_VARIABLES_HPP
